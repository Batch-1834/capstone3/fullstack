import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col } from "react-bootstrap";

import UserContext from "../UserContext";

export default function ProductView(){

	//To check if their is already a logged in user. Change the button from "Order" to "Login" if the user is not logged in.
	const { user } = useContext(UserContext);

	//Allow us to gain access to methods that will redirect a user to different page after enrolling a course.
	const navigate = useNavigate();

	// "useParams" hook allows us to retrieve the productId passed via URL.
	const { productId } = useParams();

	// Create state hooks to capture the information of a specific course and display it in our application.
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const order = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/addOrder`,{
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Succesfully ordered!",
					icon: "success",
					text: "You have successfully ordered for this product."
				})
				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() =>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className="cardHighlight">
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
						{/*	<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8 am - 5 pm</Card.Text>*/}
							<div className="d-grid gap-2">
							{
								(user.id !== null)
								?
								<Button variant="danger" size="lg" onClick={() => order(productId)}>Order</Button>
								:
								<Button as={Link} to="/login" variant="danger" size="lg">Login to Order</Button>
							}
							</div>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}