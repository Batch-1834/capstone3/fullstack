import Banner from "../components/Banner";

import Highlights from "../components/Highlights"
// import Carousel from 'react-bootstrap/Carousel';

export default function Home(){
	
	const data = {
		title: "PAYDAY SALE",
		content: "from August 14-15",
		destination: "/products",
		label: "Shop Now"
	}
	
	return(
		<>
			<Banner data={data}/>
			<Highlights />
		
		</>
	)
}

// function IndividualIntervalsExample() {
//   return (
//     <Carousel>
//       <Carousel.Item interval={1000}>
//         <img
//           className="d-block w-100"
//           src="..public/img/ecoms.jpg"
//           alt="First slide"
//         />
//         <Carousel.Caption>
//           <h3>First slide label</h3>
//           <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
//         </Carousel.Caption>
//       </Carousel.Item>
//       <Carousel.Item interval={500}>
//         <img
//           className="d-block w-100"
//           src="../public/img/pic3a.jpeg"
//           alt="Second slide"
//         />
//         <Carousel.Caption>
//           <h3>Second slide label</h3>
//           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
//         </Carousel.Caption>
//       </Carousel.Item>
//       <Carousel.Item>
//         <img
//           className="d-block w-100"
//           src="../public/img/cart.jpg"
//           alt="Third slide"
//         />
//         <Carousel.Caption>
//           <h3>Third slide label</h3>
//           <p>
//             Praesent commodo cursus magna, vel scelerisque nisl consectetur.
//           </p>
//         </Carousel.Caption>
//       </Carousel.Item>
//     </Carousel>
//   );
// }

// export default IndividualIntervalsExample;