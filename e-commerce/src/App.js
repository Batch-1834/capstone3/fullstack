import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";
// import Banner from "./components/Banner";

import AddProduct from "./pages/AddProduct";
import AdminDashboard from "./pages/AdminDashboard";
import EditProduct from "./pages/EditProduct";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import Cart from "./pages/Cart";
import { CartProvider } from "react-use-cart";
import {Container} from "react-bootstrap";
import './App.css';

function App() {

  // Global state
  // To store the user information and will be used for validation if a user is already logged in on the app or not.
  const [user, setUser] = useState({
          // null
    // email: localStorage.getItem("email")

    id: null,
    isAdmin: null

  })

  // we can check the changes in our User state.
  console.log(user);

  // Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(() =>{
    console.log(user);
    console.log(localStorage);
  }, [user])

  // To update the User state upon page load if a user already exist.
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
        headers:{
            Authorization: `Bearer ${localStorage.getItem("token")}`
        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        // set the user states values with the user detaild upon successful login
        if(typeof data._id !== "undefined"){
                  setUser({
                    //undefined
                    id: data._id,
                    isAdmin: data.isAdmin
                  })
              }
              else{
                  setUser({
                      //undefined
                      id: null,
                      isAdmin: null
                    })
              }


        // This will be set to the user state.
        setUser({
            id: data._id,
            isAdmin: data.isAdmin
        })
    })

  }, [])

  return (

    // In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component

    // All other components/pages will be contained in our main component: <App />

    // ReactJS does not like rendering two adjacent elements. instead the adjacent element must be wrapped by a parent element/react fragment
    
    //ReactJS is a single page application (SPA)
    // Router component is used to wrap around all components which will have access to our routing system.
   
  // We stroe the information in the context by providing the information using the corresponding "Provider" component and passing the information via the "value" prop/attribute.

    <UserProvider value = {{user, setUser, unsetUser}}>
      {/*Router component is use to wrap arround all components which will have access to our routing sytem*/}
      <Router>
        <AppNavbar />
        <Container fluid>
          {/*Routes holds all our Route components.*/}
          <Routes>
          {/*
              - Route assigns an endpoint and displays the appropriate page component for that endpoint.
              - "path" attribute assigns the endpoint.
              - "element" attribute assigns page component to be displayed at the endpoint.
          */}
              <Route exact path = "/" element={<Home />}/>
              <Route exact path = "/admin" element={<AdminDashboard />}/>
              <Route exact path = "/addProduct" element={<AddProduct />}/>
              <Route exact path = "/cart" element={<Cart />}/>
              
              <Route exact path = "/editProduct/:productId" element={<EditProduct />}/>
              <Route exact path = "/products" element={<Products />}/>
              <Route exact path = "/products/:productId" element={<ProductView />}/>
              <Route exact path = "/register" element={<Register />}/>
              <Route exact path = "/home" element={<Home />}/>
              <Route exact path = "/login" element={<Login />}/>
              <Route exact path = "/logout" element={<Logout />}/>
              <Route exact path = "*" element={<Error />}/>
                
              {/*<Products />*/}
              {/*<Register />*/}
              {/*<Login />*/}
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
  }

  

// // function App(){
// //   return (
//       <>

//         <CartProvider>
//           <Home />
//           <Cart />
//         </CartProvider>

//       </>
//   );
// }


/*
  Mini Activity

  1. Create a ProductCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
    - The course name should be in the card title.
    - The description and price should be in the card subtitle.
    - The value for description and price should be in the card text.
    - Add a button for Enroll.
  2. Render the ProductCard component in the Home page.
  3. Take a screenshot of your browser and send it in the batch hangouts

*/

export default App;

