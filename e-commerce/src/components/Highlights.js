import {Row, Col, Card} from "react-bootstrap";

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
		    <Col xs={12} md={4}>
		    	
		    	<Card className="cardHighlight p-3">
		    		{/*<Card style={{ width: `18rem`}}>*/}
		            <Card.Img variant="top" src="../public/img/pic5.jpg" />
		            <Card.Body>
		                <Card.Title>
		                
		                	<h2>SUMMER saya PROMO</h2>
		                </Card.Title>
		                <Card.Text className="text">
		                    SUMMER fun for the entire family or friends and make the most out of the perfect sunny weather. Pack your adventure full of memories whether it would be on the beach, well no worries because at PRODEN we have different lighting needs such as flashlights, emergency lamps and worklights that will be handy only at a valuable offer. 
		                </Card.Text>
		            </Card.Body>
		        </Card>
		        
		   
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>FASTER Deliveries</h2>
		                </Card.Title>
		                <Card.Text className="text">
		                    Our people working with us is our greatest asset and our customers are the reason we exist. Here at PRODEN we make sure that quality products are in tip-top quality once delivered to our clients. Quality is at the top of our priority, thus we have partnered with credible logistics providers such as FASTCargo and Express Delivery.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>MONEY Back Guarantee</h2>
		                </Card.Title>
		                <Card.Text className="text">
		                    Our products are ISO certified and are continuously check for defects and damages. Customers are assured that we only source our raw materials from verified sources with quality at hand. Moreover, 30 percent of our workforce are in research and development, checkers and test evaluation lead engineers to audit and find solutions.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>
	)
}
