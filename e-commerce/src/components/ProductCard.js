import React from "react";
import { Link } from "react-router-dom";
import { Card, Button, Col, Stack} from "react-bootstrap";
import { useCart } from "react-use-cart";

import { useState, useEffect } from "react"

const ProductCard = (props) => {

	const { addItem } = useCart()

	// console.log(props.productProp._id)

	const [myProduct, setMyProduct] = useState()


	useEffect(() => {
	setMyProduct({
	  id: props.productProp._id,
	  imageURL: props.productProp.imageURL,
	  name: props.title,
	  description: props.description,
	  price: props.price
	});
	}, []);

	// console.log(myProduct)

	return(
		
		<Col xs={12} md={3}>
		    <Card border="danger" className="cardHighlight">
		    	<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=${props.productProp.imageURL}`} className="img-fluid my-image"/>
		        <Card.Body>
		           	<Card.Header>{props.name}</Card.Header>
		            <Card.Text style={{fontSize:"16px"}}>
		               {props.description}
		            </Card.Text>  
		        </Card.Body>
		        
		        <Card.Footer className="text-center p-3">
		        	<div className="container">
		                <Card.Text className="fw-bold text-start">
	    	               Price: &#8369;{props.price}<br/>
	    	               Stocks: {props.stocks}
		    	        </Card.Text>
		        	</div>

		        	<Button className="btn btn-primary" onClick={() => addItem(myProduct)}>Add to Cart</Button>
		        	<Button as={Link} to={`/products/${props._id}`} size="lg" className="margin-left" variant="danger">Buy Now</Button>
		        </Card.Footer>
		    </Card>
		</Col>

		




	)
}

export default ProductCard