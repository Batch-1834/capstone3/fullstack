// import Navbar from "react-bootstrap/Navbar";
// import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import {useState, useContext} from "react";
// import logo from "../public/img/pic6.jpeg";
import UserContext from "../UserContext"

import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){

	const {user} = useContext(UserContext);

		// const myDesign = {
	// 	color: "white",
	// 	backgroundColor: "Crimson"
	// };


	// State hook to store the information stored in the login page.
									// null
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	return(
		 <Navbar className="header" expand="lg">
		    {/*<Navbar className="text-header">  */} 
		        <Navbar.Brand className="ms-1" as={Link} to="/" >
		        	<img src="./img/pic6.jpeg" ms-auto width="50" height="50" alt="logo" / ></Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		    		{/*className is used instead of "class", to specify a CSS classes*/}
		          <Nav className="ms-auto" defaultActiveKey="/">
		            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>

		            {/*
						user.email is causing an error when the local storage is cleared because the initial state of user is change
		            */}
			            {
			            	(user.isAdmin)
			            	?
			            	<Nav.Link as={Link} to="/admin" eventKey="/admin">Admin Dashboard</Nav.Link>
			            	:
			            	<Nav.Link as={Link} to="/products" eventKey="/products">Product</Nav.Link>
			            }
		           {
		           		(user.id !== null)
		           		?
		           			<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
		           		:
		           			<>	
		           				<Nav.Link as={Link} to="/login" eventKey="/login">Sign up</Nav.Link>
		           				<Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
		           			</>
		           }
		            
		          </Nav>
		        </Navbar.Collapse>
		 	
		 </Navbar>
		

	)
}