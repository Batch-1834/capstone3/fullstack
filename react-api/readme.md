# Booking API Overview
## Initial Set-up
* Change the MongoDB Atlas database connection string in the backend to ensure connectivity to the correct personal database.

* Import the WDC028_CSP2.postman_collection.json in your postman application.

## Store the following data in your database
### User credentials (User Request > User Registration)
* Admin User
	* email : "denorp@mail.com"
	* password : "denorp123"
	* isAdmin : true (default value: manually change this one in MongoDB)

* Customer User
	* email : "test@mail.com"
	* password : "test123"
	* isAdmin : false (default value)

* Customer User
	* email : "seller@mail.com"
	* password : "seller123"
	* isAdmin : false (default value)

* Customer User
	* email : "buyer@mail.com"
	* password : "buyer123"
	* isAdmin : false (default value)

* Customer User
	* email : "buyer@mail.com"
	* password : "buyer123"
	* isAdmin : false (default value)

### Course Creation (Course Request > Create a course)
* Product 1
	* name : "BOSE Soundlink Revolve"
	* description : "More sound all around-Engineered to deliver true 360°..."
	* price : 2000
	* stocks : 20
	* isActive : true
	* createdOn : 2022-07-05T02:42:06.733+00:00
* Product 2
	* name : "JBL Xtreme 3"
	* description : Powerful JBL Original PRO sound: Four drivers and two JBL Bass Radiato...
	* price : 1000
	* stocks : 10
	* isActive : true
	* createdOn : 2022-07-05T02:42:06.733+00:00
* Product 3
	* name : "Harman Kardon Onyx Studio 6"
	* description : "Wireless Bluetooth Speaker - IPX& Waterproof Extra Bass Sound System w..."
	* price : 5000
	* stock : 10
	* isActive : true
	* createdOn : 2022-07-05T02:42:06.733+00:00
* Product 4
	* name : "Sony X-Series"
	* description : "Portable bluetooth party speaker, X-Balanced Speakers for powerful bas..."
	* price : 4000
	* stock : 0
	* isActive : false
	* createdOn : 2022-07-05T02:42:06.733+00:00
* Product 5
	* name : "AKG"
	* description : "Portable bluetooth party speaker, X-Balanced Speakers for powerful bas..."
	* price : 500
	* stock : 100
	* isActive : true
	* createdOn : 2022-07-07T12:01:05.944+00:00
* Product 6
	* name : "Edifier"
	* description : "Portable bluetooth party speaker, X-Balanced Speakers for powerful bas..."
	* price : 1000
	* stock : 100
	* isActive : true
	* createdOn : 2022-07-07T12:48:18.985+00:00

### Booking API Features
* User email request
* User Registration (admin and user only)
* User Authentication (admin only)
* User set as admin (admin only)
* User Checkout (user only)
* Get my orders (admin and user only)
* Get all orders (admin only)
* Retrieve all users (admin only)
* Create a product (Admin only)
* Retrieve a product (user only)
* Retrieve all active products (admin only)
* Update a product (admin only)
* Archive a product (admin only)
* Check all inactive (admin only)
