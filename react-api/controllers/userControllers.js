const User = require("../models/User");

const bcrypt = require("bcrypt");
const auth = require("../auth");
const Order = require("../models/Order");
const Product = require("../models/Product");

// Check if email exists
module.exports.checkEmailExists = (reqBody) =>{
	return User.find({email: reqBody.email}).then(result =>{

		// Result is equal to an empty array ([]);
		// If result is greater than 0, user is found/already exists.
		if(result.length > 0){
			return true;
		}

		// No duplicate email found
		else{
			return false;
		}
	});
}

// Register user

module.exports.registerUser= (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result != null && result.email == reqBody.email){
			return "Duplicate email found!";
		}
		else{
			if(reqBody.email != "" && reqBody.password != ""){
				let newUser = new User({
					firstName: reqBody.firstName,
					lastName: reqBody.lastName,
					email: reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10)
				});
				return newUser.save().then((user, error) =>{
						if(error){
							return "Registration failed!";
						}
						else{
							console.log(newUser);
							return "Successfully registered!";
						}
					})
			}
			else{
				return "Both email and password must be provided!"; 
			}
		}
	})
}

// Controller for the user details Details)
module.exports.userData = (dataUserLogin) => {
	return User.findById(dataUserLogin.userId).then(result =>{
		return result;
		// return `Complete Name: ${result.completeName}\nEmail: ${result.email}\n`;
	})
}


// Retrieve all users

module.exports.retrieveAllUsers = () =>{
	// The "then" method is used  to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/postman.
	return User.find({}).then(result => result);
}
// user login

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{

		// User doesn't exist
		if(result == null){
			return false;
		}

		// User exist
		else{
			// Syntax: bcrypt.compareSync(data, encrypted);
			// reqBody.password & result.password
			// bcrypt.compareSync() return Boolean
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				// If the password match the result.
				if(isPasswordCorrect){
					return {access: auth.createAccessToken(result)}
				}
				// Password do not match
				else{
					return false;
				}
		}
	})
}

// User status update

module.exports.updateUserStatus = (userId) =>{
	let updatedStatus = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(userId, updatedStatus).then((updateStatus, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

//Create Order - Customer only

module.exports.createOrder = async (data) =>{
let newOrder 
	let isOrderUpdated = await Order.findById(data.userId).then(orderResult =>{
		newOrder = new Order({
			userId: data.userId,
			productId: data.productId,
			price: data.price,
			quantity: data.quantity,
			total: data.total
		})
		return newOrder.save().then((order, error) =>{
			if(error){
				return "Your order has been placed"
				}
			else{
				return true;
			}	
		})		
				
		})
	let isProductUpdated = await Product.findById(data.productId).then(product =>{
		product.stocks = product.stocks - data.quantity;
		return product.save().then((stocks, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	let isPriceUpdated = await Product.findById(newOrder.productId).then(priceResult =>{
		newOrder.price = priceResult.price;
		newOrder.total = priceResult.price * newOrder.quantity;
		return newOrder.save().then((order, error)=>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	console.log(newOrder);
	if(isOrderUpdated && isProductUpdated && isProductUpdated){
		return true;
	}
	else{
		return false;
	}

}


// Retrieve User’s orders (Customer) 
// module.exports.getUserOrders = (data) =>{
// 	return Order.find({userId: data}).then(result => result);
// }

module.exports.getUserOrders = (data) =>{
	return Order.find({userId: data.userId}).then(result => result);
}
	
// Retrieve all orders (Admin only)
module.exports.getAllOrders = () =>{
	return Order.find({}).then(result => result);
}




