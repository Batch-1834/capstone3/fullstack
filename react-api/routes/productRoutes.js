const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Route for retrieving all the products
router.get("/all", auth.verify, (req, res) =>{
	productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Route for creating a product - only admin can add product

router.post("/", auth.verify, (req,res) =>{

	const userData = auth.decode(req.headers.authorization);

	/*if(userData.isAdmin){*/

	productControllers.addProduct(req.body).then(resultFromController =>res.send(resultFromController));
	// }
	// else{
	// 	return res.send("You don't have persmission to access this page");
	// }	
})

// Route for retrieving a specific product

router.get("/:productId", (req, res) =>{
	console.log(req.params.productId);

	productControllers.getProduct(req.params.productId).then(resultFromController =>res.send(resultFromController));
})

// Route for retrieving all active products
router.get("/", (req, res) =>{
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for updating a product

router.put("/:productId", auth.verify, (req, res) =>{

	productControllers.updateProduct(req.params.productId, req.body).then(resultFromController =>res.send(resultFromController));
})

// Route to Archive a product
router.patch("/:productId/archive", auth.verify, (req, res) =>{
	productControllers.archiveProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})

// retrieve all inactive
router.get("/", (req, res) => {
	productControllers.checkInActiveProducts().then(resultFromController => res.send(resultFromController));
	
})


module.exports = router;


