// Require modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
// const orderRoutes = require("./routes/orderRoutes");

// This creates a server
const app = express();
const port = 4000;

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@myfirstcluster.0dx9jyq.mongodb.net/react-ecommerce-app?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.log.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the database."))


// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Listening to port
// This syntax will allow flexibility when using the application locally or as hosted application (online).
app.use("/users", userRoutes);
app.use("/products", productRoutes);
// app.use("/courses", courseRoutes);

app.listen(process.env.PORT || port, () =>{
	console.log(`API is now online on port ${process.env.PORT || port}`);
})




